using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AttackState : IState<Enemy>
{
    //[SerializeField] private NavMeshAgent navMeshAgent;
    public void OnEnter(Enemy bot)
    {
        //navMeshAgent = bot.GetComponent<Enemy>().navMeshAgent;
        bot.Attack();
    }

    public void OnExecute(Enemy bot)
    {
        if (bot.targets.Count <= 0) 
        {
            bot.ChangeState(new PatrolState());
        }
    }

    public void OnExit(Enemy bot)
    {

    }

}
