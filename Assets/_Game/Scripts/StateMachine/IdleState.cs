using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IdleState : IState<Enemy>
{
    //[SerializeField] private NavMeshAgent navMeshAgent;
    float randomTime;
    float timer;
    public void OnEnter(Enemy bot)
    {
        //navMeshAgent = bot.GetComponent<Enemy>().navMeshAgent;
        bot.ChangeAnim("idle");
    }

    public void OnExecute(Enemy bot)
    {
        if (bot.targets.Count > 0)
        {
            bot.ChangeState(new PatrolState());
        }
    }

    public void OnExit(Enemy bot)
    {

    }

}
