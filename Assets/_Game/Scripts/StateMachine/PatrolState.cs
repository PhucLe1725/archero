using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrolState : IState<Enemy>
{
    [SerializeField] private NavMeshAgent navMeshAgent;
    public void OnEnter(Enemy bot)
    {
        navMeshAgent = bot.GetComponent<Enemy>().navMeshAgent;
        bot.ChangeAnim("run");
    }

    public void OnExecute(Enemy bot)
    {
        Character target = bot.FindNearestTarget();
        if (target != null)
        {
            bot.SetDestination(target.transform.position);
            if (bot.IsDestionation)
            {
                bot.SetDestination(bot.transform.position);
                bot.ChangeState(new AttackState());
            }
        }
        else
        {
            bot.ChangeState(new IdleState());
        }
    }

    public void OnExit(Enemy bot)
    {
       
    }

}
