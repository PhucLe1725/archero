using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IState<T>
{
    void OnEnter(T bot);
    void OnExecute(T bot);
    void OnExit(T bot);
}
