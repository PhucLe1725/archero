using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : Character
{
    private IState<Enemy> currentState;
    //cache transform de toi uu hieu nang
    [SerializeField] protected Transform tf;
    //keo navmesh agent vao
    [SerializeField] public NavMeshAgent navMeshAgent;
    //luu diem muc tieu se di den
    private Vector3 destination;

    //property tra ve ket qua xem la da toi diem muc tieu hay chua
    public bool IsDestionation => Vector3.Distance(tf.position, destination + (tf.position.y - destination.y) * Vector3.up) < 5f;

    //set diem den
    public void SetDestination(Vector3 destination)
    {
        this.destination = destination;
        navMeshAgent.SetDestination(destination);
        //Debug.Log($"{destination.x} - {destination.y} - {destination.z}");
    }


    private void Start()
    {
        OnInit();
    }

    // Update is called once per frame
    void Update()
    {
        if (targets.Count > 0)
        {
            Character nearestTarget = FindNearestTarget();
            if (nearestTarget != null)
            {
                Vector3 targetDirection = nearestTarget.transform.position - transform.position;
                Quaternion targetRotation = Quaternion.LookRotation(targetDirection);

                transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
            }
        }
        if (currentState != null)
        {
            currentState.OnExecute(this);
        }
        
    }

    public void ChangeState(IState<Enemy> state)
    {
        if (currentState != null)
        {
            currentState.OnExit(this);
        }

        currentState = state;

        if (currentState != null)
        {
            currentState.OnEnter(this);
        }
    }

    public override void OnInit()
    {
        base.OnInit();
        ChangeState(new IdleState());
    }
    protected override void OnDeath()
    {
        ChangeAnim("die");
        Invoke(nameof(OnDespawn), 2f);
    }
    public void Attack()
    {
        ChangeAnim("attack");
    }
}
