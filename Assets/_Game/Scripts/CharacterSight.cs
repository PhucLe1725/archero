using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSight : MonoBehaviour
{
    [SerializeField] Character character;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            Character target = other.GetComponent<Character>();
            if (!target.IsDead)
            {
                character.AddTarget(target);
            }
        }
        if (other.CompareTag("Player"))
        {
            Character target = other.GetComponent<Character>();
            if (!target.IsDead)
            {
                character.AddTarget(target);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            Character target = other.GetComponent<Character>();
            character.RemoveTarget(target);
        }
        if (other.CompareTag("Player"))
        {
            Character target = other.GetComponent<Character>();
            character.RemoveTarget(target);
        }
    }
}
