using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player;
    public Vector3 localEulerAngles;
    public Vector3 position;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player"); // The player
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Euler(localEulerAngles);
        transform.position = new Vector3(player.transform.position.x + position.x, player.transform.position.y + position.y, player.transform.position.z + position.z);
    }
}
