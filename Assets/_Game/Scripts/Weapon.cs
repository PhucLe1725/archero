using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] Renderer mesh;
    public BulletType bulletType;
    public BulletData bulletData;

    public void Fire(Vector3 playerLookDirection, Transform attackPoint)
    {
        Bullet curWeaponInstance;
        curWeaponInstance = Instantiate(bulletData.GetBullet(bulletType), attackPoint.position, attackPoint.rotation);
        curWeaponInstance.SetDirect(playerLookDirection * curWeaponInstance.bulletSpeed);
    }
}
