using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BulletType
{
    Knife = 0,
    Hammer = 1,
    Z = 2
}
[CreateAssetMenu(fileName = "BulletData", menuName = "ScriptableObject/BulletData", order = 2)]

public class BulletData : ScriptableObject
{
    [SerializeField] private List<Bullet> _bullet = new List<Bullet>();

    public Bullet GetBullet(BulletType type)
    {
        return _bullet[(int)type];
    }

}

