using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HatType
{
    Cowboy = 0,
    Headphone = 1,
    Horn = 2
}
[CreateAssetMenu(fileName = "HatData", menuName = "ScriptableObject/HatData", order = 4)]

public class HatData : ScriptableObject
{
    [SerializeField] private List<Hat> _hat = new List<Hat>();

    public Hat GetHat(HatType type)
    {
        return _hat[(int)type];
    }

}

