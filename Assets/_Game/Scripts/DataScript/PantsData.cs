using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PantsType
{
    Batman = 0,
    Skull = 1
}
[CreateAssetMenu(fileName = "PantsData", menuName = "ScriptableObject/PantsData", order = 5)]

public class PantsData : ScriptableObject
{
    [SerializeField] private List<Material> _pants = new List<Material>();

    public Material GetPants(PantsType type)
    {
        return _pants[(int)type];
    }

}