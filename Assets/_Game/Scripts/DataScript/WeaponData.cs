using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponType
{
    Knife = 0,
    Hammer = 1,
    Z = 2
}
[CreateAssetMenu(fileName = "WeaponData", menuName = "ScriptableObject/WeaponData", order = 1)]

public class WeaponData : ScriptableObject
{
    [SerializeField] private List<Weapon> _weapon = new List<Weapon>();
    [SerializeField] private List<ListMat> _listMat = new List<ListMat>();

    public Weapon GetWeapon(WeaponType type)
    {
        return _weapon[(int)type];
    }
    public Material[] GetMats(WeaponType type)
    {
        return _listMat[(int)type].Materials;
    }

}

[System.Serializable]
public class ListMat
{
    [SerializeField] Material[] materials;
    public Material[] Materials => materials;

}
