using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ColorType
{
    Blue = 0,
    Brown = 1,
    Yellow = 2,
    Green = 3
}
[CreateAssetMenu(fileName = "ColorData", menuName = "ScriptableObject/ColorData", order = 3)]

public class SpawnScriptableObject : ScriptableObject
{
    [SerializeField] private List<Material> mats = new List<Material>();

    public Material GetMaterial(ColorType type)
    {
        return mats[(int)type];
    }

}
