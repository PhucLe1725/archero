﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Texture2DToMaterials : MonoBehaviour
{
    public Texture2D texture; // Texture2D bạn muốn sử dụng
    public Material targetMaterial; // Material mà bạn muốn thiết lập Texture2D cho nó

    void Start()
    {
        if (targetMaterial != null && texture != null)
        {
            targetMaterial.mainTexture = texture;
        }
        else
        {
            Debug.LogWarning("Texture2D or target Material is not assigned.");
        }
    }
    public void ChangeTexture(Texture2D newTexture)
    {
        if (targetMaterial != null && newTexture != null)
        {
            targetMaterial.mainTexture = newTexture;
        }
        else
        {
            Debug.LogWarning("Texture2D or target Material is not assigned.");
        }
    }
}
