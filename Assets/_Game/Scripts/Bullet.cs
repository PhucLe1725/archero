using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] Renderer mesh;
    [SerializeField] Rigidbody rb;
    [SerializeField] public float bulletSpeed = 50f;

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Enemy")
        {
            collision.GetComponent<Character>().OnHit(30f);
            OnDespawn();
        }
        if (collision.tag == "Wall")
        {
            OnDespawn();
        }
    }
    public void OnDespawn()
    {
        Destroy(gameObject);
    }

    internal void SetDirect(Vector3 vector3)
    {
        rb.velocity = vector3;
    }
}
