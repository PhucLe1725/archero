﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] protected Animator animator;
    private float hp;
    protected string currentAnim;

    public List<Character> targets = new List<Character>();

    public Character target; // Đối tượng mục tiêu mà bạn muốn xoay tới
    public float rotationSpeed = 5f; // Tốc độ xoay


    public bool IsDead => hp <= 0;

    private void Start()
    {
        OnInit();
    }

    public virtual void OnInit()
    {
        hp = 100f;
    }

    public virtual void OnDespawn()
    {
        Destroy(gameObject);
    }
    protected virtual void OnDeath()
    {
        ChangeAnim("die");
        targets.Clear();
        Invoke(nameof(OnDespawn), 2f);
    }
    public void ChangeAnim(string animName)
    {
        if (currentAnim != animName)
        {
            animator.ResetTrigger(currentAnim);
            currentAnim = animName;
            animator.SetTrigger(currentAnim);
        }
    }

    public void OnHit(float damage)
    {
        if (!IsDead)
        {
            hp -= damage;
            if (IsDead)
            {
                hp = 0;
                OnDeath();
            }
        }
    }
    public virtual void AddTarget(Character target)
    {
        targets.Add(target);
    }

    public virtual void RemoveTarget(Character target)
    {
        targets.Remove(target);
        this.target = null;
    }
    protected void ClearTarget()
    {
        targets.Clear();
    }
    public Character FindNearestTarget()
    {
        Character nearest = null;
        float nearestDistance = Mathf.Infinity;
        Vector3 currentPosition = transform.position;

        foreach (Character target in targets)
        {
            if (target != null)
            {
                float distance = Vector3.Distance(target.transform.position, currentPosition);
                if (distance < nearestDistance)
                {
                    nearestDistance = distance;
                    nearest = target;
                }
            }
            else
            {
                RemoveTarget(target);
            }
        }

        return nearest;
    }
}
